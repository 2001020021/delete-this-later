require('dotenv').config()
const express       = require('express')
const res           = require('express/lib/response')
const app           = express()
const PORT         = 5000
const jwt          = require('jsonwebtoken')

const posts = [
    {
        username: 'Kyle',
        title: 'Post 1'
    },
    {
        username: 'Keyy',
        title: 'Post 2'
    }
]

app.use(express.json());

app.get('/posts', (req, res) => {
    res.json(posts)
})

app.post('/login', (req, res) => {
    
    const username = req.body.username
    const user = {
        name: username
    }  

    const access_token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET)
    res.json({ access_token: access_token })
})

app.listen(PORT, (req, res) => {
    console.log(`Server is running in ${PORT}`);
})